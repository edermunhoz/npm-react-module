export type Attributes = {
  borderRadius: string
}

const attributes: Attributes = {
  borderRadius: '30px',
}

export default attributes
