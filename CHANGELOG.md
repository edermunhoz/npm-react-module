# [1.4.0](https://gitlab.com/emunhoz1/npm-react-module/compare/v1.3.0...v1.4.0) (2020-07-22)


### Features

* add new button style ([9fe25dc](https://gitlab.com/emunhoz1/npm-react-module/commit/9fe25dc8898015c9f12734b3b89707373d5fc52c))

# [1.3.0](https://github.com/emunhoz/npm-react-module/compare/v1.2.0...v1.3.0) (2020-07-16)


### Features

* add release to package.json ([fddab4d](https://github.com/emunhoz/npm-react-module/commit/fddab4d642390898f9ce8044635fbf99bb77b68b))
* add release to package.json ([e00c88f](https://github.com/emunhoz/npm-react-module/commit/e00c88f8caa74f83e1b4668c3dc5076e6eb69128))
